from typing import Any, Dict

import pytest


def test_list_shoes(client):
    response = client.get("/api/shoes")
    assert response.status_code == 200

    result = response.json
    assert type(result) == list
    assert len(result) == 3

    shoe: Dict[str, Any] = result[0]
    required_keys = ["id", "model", "minPrice", "maxPrice"]
    for key in shoe.keys():
        assert key in required_keys


@pytest.mark.parametrize("id", (1, 2, 3))
def test_shoe_price(client, id):
    response = client.get(f"/api/shoe-price/{id}")
    assert response.status_code == 200

    result: Dict[str, Any] = response.json
    required_keys = ["originalPrice", "finalPrice"]
    for key in result.keys():
        assert key in required_keys


@pytest.mark.parametrize("id", (4, "xxx"))
def test_shoe_price_not_found(client, id):
    response = client.get(f"/api/shoe-price/{id}")
    assert response.status_code == 404
