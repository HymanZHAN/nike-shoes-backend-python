from shoe_price.models import Shoe


def test_shoe_discount():
    shoe = Shoe(
        id=1,
        model="Test",
        min_price=100,
        max_price=200,
        original_price=180,
        discount_rate=0.8,
    )

    assert shoe.final_price == 180 * 0.8


def test_shoe_reprice():
    shoe = Shoe(
        id=1,
        model="Test",
        min_price=100,
        max_price=200,
        original_price=180,
        discount_rate=0.8,
    )

    shoe.reprice(new_price=200)

    assert shoe.original_price == 200
    assert shoe.final_price == 200 * 0.8
