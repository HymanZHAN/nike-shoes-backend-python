import pytest

from shoe_price import create_app
from shoe_price.models import Shoe


@pytest.fixture
def db():
    return [
        Shoe(id=1, model="Nike Air Max 95 SE", min_price=120, max_price=150),
        Shoe(id=2, model="Nike Air Max 97 SE", min_price=5, max_price=150),
        Shoe(id=3, model="Nike Air Max Pre-Day", min_price=120, max_price=160),
    ]


@pytest.fixture
def app(db):
    """Create and configure a new app instance for each test."""
    # create the app with common test config
    app = create_app(data=db)
    app.config.update({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    """A test client for the app."""
    return app.test_client()
