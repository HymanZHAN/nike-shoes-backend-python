from .shared.dtos.base import CamelOrmBase


class RandomPriceDto(CamelOrmBase):
    shoe_price: float


class ShoeDto(CamelOrmBase):
    id: int
    model: str
    min_price: float
    max_price: float


class ShoeWithPriceDto(ShoeDto):
    original_price: float
    final_price: float


class ShoePricesDto(CamelOrmBase):
    original_price: float
    final_price: float
