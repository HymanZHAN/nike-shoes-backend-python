from flask import abort, current_app
from flask_restful import Api, Resource  # type:ignore
from pydantic import ValidationError

from shoe_price.excs import ShoeNotFound

from .services import get_shoe_prices_by_id, list_shoes

api = Api()


class Shoes(Resource):
    def get(self):
        db = current_app.config["db"]
        shoes = list_shoes(db)
        return [shoe.dict(by_alias=True) for shoe in shoes]


class ShoePrice(Resource):
    def get(self, shoe_id: int):
        db = current_app.config["db"]
        try:
            shoe_prices = get_shoe_prices_by_id(db, shoe_id)
            return shoe_prices.dict(by_alias=True)
        except ShoeNotFound:
            abort(404, "Shoe not found")
        except ValidationError:
            abort(500, "Internal Error")


api.add_resource(Shoes, "/api/shoes", endpoint="shoes")
api.add_resource(ShoePrice, "/api/shoe-price/<int:shoe_id>", endpoint="shoe_price_by_id")
