from typing import List

import requests

from shoe_price.excs import ShoeNotFound

from .dtos import RandomPriceDto, ShoeDto, ShoePricesDto
from .models import Shoe


def list_shoes(db) -> List[ShoeDto]:
    return [ShoeDto.from_orm(shoe) for shoe in db["shoes"]]


def get_shoe_prices_by_id(db, shoe_id: int) -> ShoePricesDto:
    shoe = get_shoe_by_id(db, shoe_id)
    price = fetch_random_price_for_shoe(db, shoe_id)
    shoe.reprice(price)
    return ShoePricesDto.from_orm(shoe)


def get_shoe_by_id(db, shoe_id: int) -> Shoe:
    for shoe in db["shoes"]:
        if shoe.id == shoe_id:
            return shoe
    raise ShoeNotFound


def fetch_random_price_for_shoe(db, shoe_id: int):
    shoe = get_shoe_by_id(db, shoe_id)
    url = f"http://localhost:9090/fetch-random-price/{shoe.id}"
    response = requests.get(url)
    data = response.json()
    price = RandomPriceDto.parse_obj(data).shoe_price
    return price
