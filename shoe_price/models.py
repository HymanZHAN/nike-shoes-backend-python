from dataclasses import dataclass, field


@dataclass
class Shoe:
    id: int
    model: str
    min_price: float
    max_price: float

    final_price: float = field(init=False)
    original_price: float = 0
    discount_rate: float = 0.6

    def __post_init__(self):
        self.final_price = self.original_price * self.discount_rate

    def reprice(self, new_price: float):
        self.original_price = new_price
        self.final_price = self.original_price * self.discount_rate
