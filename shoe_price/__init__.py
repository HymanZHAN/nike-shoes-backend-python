import os

from dotenv import load_dotenv
from flask import Flask

from shoe_price.api import api as shoe_price_api
from shoe_price.db import shoes

APP_ROOT = os.path.join(os.path.dirname(__file__), "..")
dotenv_path = os.path.join(APP_ROOT, ".env")
load_dotenv(dotenv_path)


def create_app(data=None):
    app = Flask(__name__)

    if not data:
        data = shoes
    if "db" not in app.config:
        app.config["db"] = {"shoes": data}

    shoe_price_api.init_app(app)

    return app
