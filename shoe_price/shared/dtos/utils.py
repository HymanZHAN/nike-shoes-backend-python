from typing import Any, Callable, Dict, Optional

import orjson


def to_camel(string: str) -> str:
    first_word = string.split("_")[0]
    remaining_words = "".join(word.capitalize() for word in string.split("_")[1:])
    return first_word + remaining_words


def orjson_dumps(v: Dict[Any, Any], *, default: Optional[Callable[[Any], Any]] = None) -> str:
    # orjson.dumps returns bytes, to match standard json.dumps we need to decode
    return orjson.dumps(v, default=default).decode()
