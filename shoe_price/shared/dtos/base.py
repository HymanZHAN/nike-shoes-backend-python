import orjson
from pydantic import BaseModel

from .utils import orjson_dumps, to_camel


class CamelOrmBase(BaseModel):
    """BaseModel with `orm_mode=True` and camel case aliases"""

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        alias_generator = to_camel
        validate_assignment = True
        json_loads = orjson.loads
        json_dumps = orjson_dumps
