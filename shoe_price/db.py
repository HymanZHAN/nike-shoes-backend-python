from shoe_price.models import Shoe

shoes = [
    Shoe(id=1, model="Nike Air Max 95 SE", min_price=120, max_price=150),
    Shoe(id=2, model="Nike Air Max 97 SE", min_price=5, max_price=150),
    Shoe(id=3, model="Nike Air Max Pre-Day", min_price=120, max_price=160),
    Shoe(id=4, model="Nike Air Max 270", min_price=100, max_price=130),
    Shoe(id=5, model="Nike Renew Ride 3", min_price=180, max_price=200),
    Shoe(id=6, model="Nike Air Max 90", min_price=120, max_price=150),
]
