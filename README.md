# Nike Interview Backend Code Challenge [Python]

## Pre-requisites

- `Python` and `pip`
- API: Clone [this project](https://gitlab.com/hiring_nike_china/fetch-shoe-prices/) and start this nodejs server, following the instructions on this project. The project should run at port 9090

  - Get original price (randomly fetched) for a supplied shoe id:

    ```
    URL (GET) - http://localhost:8081/api/shoe-price/1

    Response:
    {
        "shoePrice": 147
    }
    ```

## Start the project

- Create virtual environment:

  ```shell
  pip install virtualenv
  virtualenv env
  source ./env/bin/activate
  ```

- Install dependencies

  ```shell
  pip install -r requirements.txt
  ```

- Run the Flask project

  ```shell
  flask run -h localhost -p 8081
  ```

  You can try out the API at `localhost:8081/api/shoes` or `localhost:8081/api/shoe-price/1`

## Project Description

### Changes & Improvements

The backend project is structured based on the recommended architecture from the official Flask tutorial. The application itself is packaged as its own Python package with a layered architecture, consisting the following layers:

- API Layer
- Service Layer
- Data Persistence Layer
- Domain Layer

The API layer is where Flask, the backend framework, handles requests and generate responses. The package `Flask-RESTful` is used to make the API development easier and more "RESTful".

The service layer is responsible for implementing functionalities of the application. It's the central part of the application. In the context of this project, it is responsible for interacting with the database and external APIs. By encapsulating functionalities into different services, it frees the API layer, or view layer, from handling all the business requirements and implementation details.

The data persistence layer is responsible for storing data into our data store. In this case, for the sake of simplicity, the data store is a hard-coded Python list. However, we should be able to switch to other ORMs and databases.

The domain layer is responsible for domain model definition, securing domain constraints and maintain data integrity. It's the core of the application and should depend on no external code. It describes the essential business requirements and should be constructed in a way that's agreed on by domain experts.

Other than the architecture change, the code base is configured with maintainability and code quality in mind. Type annotations are used where appropriate to make intention clearer and editor support better. Various tools such as `black`, `flake8` and `mypy` are used to maintain code quality and there are scripts to automate formatting and linting tasks. `pytest` were used to test the application with coverage report.

### Potential Improvements

- Add a ORM like SQLAlchemy with a local sqlite database.
- Improve the pytest fixture configuration for swapping in the test data with the test database.
- Add API documentation via Swagger
- Treat services as the primary tested object and write more integrated tests. In my opinion, API is a bit too high-level and model is a bit too low-level.
- Configure `commitlint` to standardize commit message.

### Doubts & Assumptions

- The API style of `localhost:8080/api/shoe-price/:shoeId` isn't really RESTful. In my opinion, the value of "shoe price" should not be considered a resource and thus should always be returned together with the corresponding shoe object. Maybe this can be accepted in the "microservices" setting, but in that case the final response should be constructed by some BFF (backend for frontend) instead of being directly returned from such a low-level API.
