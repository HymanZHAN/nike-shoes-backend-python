#!/usr/bin/env bash

set -x
set -e

mypy shoe_price

black shoe_price --check 

pflake8 shoe_price
pflake8 tests

vulture shoe_price --min-confidence 70
vulture tests --min-confidence 70
