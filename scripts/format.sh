#!/usr/bin/env bash

set -x

autoflake --remove-all-unused-imports --recursive --remove-unused-variables --in-place shoe_price --exclude=__init__.py
autoflake --remove-all-unused-imports --recursive --remove-unused-variables --in-place tests --exclude=__init__.py

isort --multi-line=3 --trailing-comma --force-grid-wrap=0 --combine-as --line-width 100 shoe_price
isort --multi-line=3 --trailing-comma --force-grid-wrap=0 --combine-as --line-width 100 tests

black --line-length=100 shoe_price
black --line-length=100 tests