#!/usr/bin/env bash

set -x
set -e

python -m pytest --cov=shoe_price --cov-report=html tests